![UNAM](https://gitlab.com/nehnemini/redes-2021-1/-/raw/87fe32dd92a47c8c9ac44d46a33351f47c033927/img/img_logoFC_2019.png)
# Redes de Computadoras 




## Proyecto: Servicios en la nube de AWS

## Objetivo:
* El alumno implementará una infraestructura de servidores en la nube para ofrecer un servicio web.
* El alumno conocerá el proceso para contratar, configurar y poner en un ambiente de producción una aplicación web completa.
* El alumno pondrá en práctica los conceptos vistos en las clases de teoría y laboratorio como protocolos HTTP, HTTPS, DNS, SSH, SMTPS de manejador de bases de datos, servidor proxy para caché, registro de un dominio, registros DNS, entre otros.


## Requerimientos

### Diagrama general de conexión

![DIAGRAM](https://gitlab.com/nehnemini/redes-2021-1/-/raw/master/img/network_project-2021-1.png)


### Obtener un nombre de dominio
* Obtener un dominio que será utilizado para todo el proyecto, por ejemplo el dominio *proyectoredes.tk*.
* Se recomienda que sea a través de un proveedor que lo ofrezca de forma gratuita como [Freenom](https://www.freenom.com), este proveedor ofrece dominios gratuitos para ciertos dominios de nivel superior o TLD (*Top Level Domain*). Otra opción es adherirse al programa [Github Student Developer Pack](https://education.github.com/pack), en el que varios proveedores ofrecen dominios gratuitos, se recomienda obtenerlo con [.tech DOMAINS](https://education.github.com/pack/redeem/tech-student).


### Contratación de servidores virtuales
* Contratación de cuatro servidores virtuales para albergar los siguientes servicios:
  * Web Server (EC2), el que tendrá asociado el dominio a su dirección IP, tendrá únicamente el servidor web. Tendrá el alias asociado www, por ejemplo *www.proyectoredes.tk*, y ofrecerá el servicio por el protocolo HTTPS.
  * Email Server (EC2), éste ofrecerá el servicio de correo electrónico usando el protocolo SMTPS. Se configurarán los registros DNS necesarios para su correcto funcionamiento, además de una buena configuración de seguridad.
  * App Server (EC2), ofrecerá el servicio de la aplicación, únicamente será accesible desde la red interna usando direcciones IP privadas.
  * Data Server (EC2), ofrecerá el servicio de almacenamiento y gestión de datos, únicamente será accesible desde la red interna usando direcciones IP privadas.
* Local DNS (Route53), se configurará el servicio de DNS local o interno, a través del servicio Route53 de AWS.
* Unícamente los servidores Web y de Correo contarán con direcciones IP públicas y estáticas, tanto IPv4 como IPv6, se deberán hacer las configuraciones necesarias para los registros DNS.

### Uso de una CDN (*Content Delivery Network*)
* Deberán configurar una CDN, se recomienda usar el servicio gratuito de Clouflare.


## Desarrollo

### Instalación y configuración del servidor Web
* Instalar un servidor web, como Apache, Nginx, etc.
* Instalar y configurar lo necesario para las conexiones a través de HTTPS.
* El sitio web deberá tener configurado un certificado para las conexiones HTTPS, para esto se usará [Let's encrypt](https://letsencrypt.org).


### Configuración de los registros DNS, de una CDN y HTTPS
* Configuración de los registros DNS necesarios, se usará el servicio gratuito de [Cloudflare](https://www.cloudflare.com). Se recomienda que en la creación y configuración de los registros de DNS de Cloudflare, se deshabilite la opción de *Proxied*, para que únicamente se use el servicio de DNS de Cloudflare. Se sugiere usar para verificar las configuraciones el comando `dig @ip_dns_público registro_DNS dominio`, por ejemplo, `dig @1.1.1.1 MX proyectoredes.tk`. Recordar habilitar la opción de *Proxied* al verificar que los registros estén correctamente configurados.
* Se deberá indicar al provedor del dominio como *.tech DOMAINS* o *Freenom*, que se usarán los DNS de Cloudflare.
* Deberá estar asociado el nombre de dominio a la dirección IP de su servidor web.
* Se podrá consultar su sitio web de forma exitosa a través de por ejemplo, *proyectoredes.tk* o *www.proyectoredes.tk*.
* También se podrá consultar su sitio web usando una dirección IPv6, es decir su registros DNS deberán estar configurados para este tipo de peticiones.
* Para el punto anterior se recomienda usar Teredo para probar la conexión del cliente hacia el sitio web a través de IPv6, si es que su ISP no provee el servicio de IPv6.
* Configurar los registros DNS necesarios, los mínimos son los siguientes:

Registros DNS                    |
---------------------------------|
Registro A (Web Server)          |
Registro A (Email Server)        |
Registro AAAA (Web Server)       |
Registro AAAA (Email)            |
Registro PTR (Web Server)        |
Registro PTR (Email Server)      |
Registro CNAME (Web Server)    	 |
Registro MX                      |
Registro TXT (datos de SPF)      | 	
Registro TXT (datos de DKIM)     |

* Cloudflare permite la configuración gratuita de hasta tres reglas de la caché de su sitio web en la CDN, deberán configurar estas reglas con base en su aplicación del sitio web. Esta configuración la pueden realizar una vez que ya tengan programada su aplicación. Se recomienda que las reglas sean para hojas de estilo (CSS), imágenes y/o programas en JavaScript.
* Se deberán hacer las configuraciones necesarias para usar la CDN de Cloudflare y seguir usando el certificado de Let's encrypt en el sitio, se recomienda el modo [Full (strict)](https://support.cloudflare.com/hc/en-us/articles/200170416-End-to-end-HTTPS-with-Cloudflare-Part-3-SSL-options) en la configuración de Cloudflare. 


Hasta este punto su sitio web ya podría ser consultado desde cualquier lugar colocando su URL en un navegador web, usando una conexión de direcciones IPv4. Debido a que aún no se realiza la aplicación, se mostrará la pantalla inicial de Apache. Las pruebas con IPv6 se pueden dejar al final del proyecto.


### Configuración de DNS local
* Configuración de resolución de nombres interna (para la VPC en la que se encuentran las instancias únicamente) a través del servicio de Route53 de AWS. Se configurará una Zona hospedad privada (*Private Hosted Zones*), es decir, será un DNS que sólo resolverá de forma interna, no permitirá las consultas DNS externas.
* Configurar los registros DNS necesarios, los mínimos son los siguientes:

Registros DNS locales o internos |
---------------------------------|
Registro A (Web Server)          |
Registro A (Email Server)        |
Registro A (App Server)          |
Registro A (DB Server)           |
Registro CNAME (Web Server)    	 |
Registro MX                      |

* Se usará el mismo dominio que se configuró previamente en el DNS externo, para la configuración de este DNS interno. Por ejemplo, tendrá los registros, www.proyectoredes.tk, db.proyectoredes.tk, app.proyectoredes.tk, etc.


### Instalación y configuración de los servidores de aplicación y de base de datos
* Instalar un manejador de bases de datos de acuerdo a las necesidades de la aplicación, puede ser MariaDB, PostgreSQL, MongoDB, etc., en el Data Server.
* La lógica de la aplicación residirá en el App Server, por lo que se tendrá que configurar el servidor web para que se conecte a este servidor en donde se estará ejecutando los programas de la aplicación y regresarán el resultado al servidor web para que este último envíe la respuesta al usuario. Es decir, se deberá de configurar un *Reverse proxy* entre el servidor web y el servidor de aplicación.	 
* Las conexiones a la base de datos desde la aplicación se harán a través de un canal cifrado.


### Programación de la aplicación.
* Al menos la aplicación consistirá en un *Login*, es decir, al menos deberá contar con autenticación de los usuarios a través de contraseñas, los usuarios y las contraseñas residirán en la base de datos, por lo que se tendrán que hacer consultas a ésta.
* Se otorgarán puntos extras sobre la calificación del proyecto si la aplicación tiene más funcionalidades. Se recomienda una aplicación sencilla que no les involucre demasiado tiempo. También pueden instalar una aplicación ya hecha como cualquier CMS (*Content Management System*).
* El lenguaje de programación, y en su caso el framework de desarrollo, se dejará a elección de cada equipo.
* Su código deberá estar en un proyecto de GitLab, y se deberá configurar este repositorio en el servidor de aplicación, tal y como se hizo en la práctica 3.


### Configuración de un servidor de correo electrónico.
* Configurar un servidor para recibir y enviar correo electrónico con su mismo dominio.
* Para este punto la seguridad cobra mayor relevancia, por lo que deberán configurar correctamente el servidor para que no sea usado por terceros para mandar spam.
* No debe estar en *open relay*.
* Se deberán de configurar también los registros DNS para correo electrónico: para dirigir el correo electrónico del dominio al servidor de correo electrónico; para verificar la propiedad del dominio; para identificar qué servidor de correo puede enviar correos a nombre del dominio configurado.
* Deberán de usar el protocolo SMTPS, IMAP y POP3.
* Deberán de configurar un cliente de correo, en un equipo propio, para la consulta del correo de su servidor, como Thunderbird, Outlook, Mail, etc.
* Configurar el servicio de SMTPS usando un certificado creado con Let's encrypt.
* Puntos extras sobre la calificación si configuran IMAPS y POP3S.
* Puntos extras sobre la calificación si instalan un sitio web para la consulta de correo electrónico, también deberá de usar HTTPS.
* Deberán solicitar a AWS que les permita el [uso del puerto TCP/25](https://aws.amazon.com/es/premiumsupport/knowledge-center/ec2-port-25-throttle/), este proceso puede tardar algunos días.
  

### Consideraciones sobre seguridad de la información
Debido a que sus servidores estarán expuestos en Internet, deberán de realizar unas configuraciones mínimas de seguridad.
* Configurar un [NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html), para que no sea necesario que los equipos de la red interna tenga una IP pública, no podrán responder conexiones iniciadas desde internet, pero sí iniciadas desde el servidor hacia internet.
* Configurar el servidor de aplicación para que solamente acepte conexiones del servidor web, y del servidor de base de datos.
* Configurar el servidor de base de datos para que solamente acepte conexiones del servidor de aplicación.
* Para la administración conectarse a través de SSH a los servidores.
* No permitir la conexión remota por SSH al usuario root.
* Crear un usuario dentro del manejador de bases de datos que solamente tenga permisos sobre la base de datos de la aplicación, no usar root para las conexiones de la aplicación.
* No almacenar en claro la contraseña de los usuarios en la base de datos.
* El servidor web no deberá de listar ni archivos ni directorios.
* Instalar y configurar fail2ban para el servicio de SSH para bloquear intentos de conexión no autorizados  que se hace por fuerza bruta.
* Programar de forma segura al menos la autenticación de usuario y contraseña, filtrando el contenido de los campos que se reciben desde el usuario para evitar un SQL injection. 
* Desinstalar servicios que no se usen en los servidores. 


## Condiciones de entrega
* El proyecto lo podrán realizar en equipos de tres alumnos.
* Entregar un breve reporte en PDF que contenga
    * Diagrama de red de los servidores, indicando direcciones IP.
    * Registros DNS configurados.
    * Resumen de las configuraciones del servidor web, servidor de aplicación, servidor de correo electrónico y base de datos.
    * Esquema de la base de datos.
    * Objetivo de la aplicación (redactado para un usuario).
    * Uso de la aplicación (breve descripción de como usarla con capturas de pantalla, pensando en un usuario).
    * Comentarios sobre el desarrollo del proyecto.
    * Evidencia que compruebe que cada requerimiento ha sido cubierto, debido a que no habrá una exposición, el documento será la única forma de demostrar que se cumplió con lo solicitado.
    * Deberán dejar funcionando sus servicios durante 15 días a partir de la fecha de entrega del proyecto.
    * Subir el documento a Moodle.
* Se podrán hacer preguntas por medio del canal de Discord destinado a ello. Si es necesario se podrá solicitar una o varias sesiones de preguntas y respuestas en los horarios de clase o del laboratorio.
* Se usará la Rúbrica para calificar el trabajo del equipo.
* NO habrá prórroga en la fecha de entrega.



## Definiciones
* CDN: es una plataforma de servidores altamente distribuida que ayuda a minimizar los retrasos en la carga de contenidos de páginas web al reducir la distancia física entre el servidor y el usuario. De esta manera, usuarios de todo el mundo puedan visualizar el mismo contenido de alta calidad sin tiempos de carga lentos. *Akamai* 

* VPC: Amazon Virtual Private Cloud (Amazon VPC) permite aprovisionar una sección de la nube de AWS aislada de forma lógica, en la que puede lanzar recursos de AWS en una red virtual que usted defina. Puede controlar todos los aspectos del entorno de red virtual, incluida la selección de su propio rango de direcciones IP, la creación de subredes y la configuración de tablas de ruteo y gateways de red. Puede usar tanto IPv4 como IPv6 en su VPC para obtener acceso a recursos y aplicaciones de manera segura y sencilla. Es fácil personalizar la configuración de red de Amazon VPC. Por ejemplo, puede crear una subred de acceso público para los servidores web con acceso a Internet y colocar los sistemas backend, como bases de datos o servidores de aplicaciones, en una subred de acceso privado sin acceso a Internet. Puede aprovechar varias capas de seguridad, que incluyen grupos de seguridad y listas de control de acceso a red, para ayudar a controlar el acceso a las instancias de Amazon EC2 desde cada subred.

* Subnet: Las subredes son las ubicaciones dentro de una VPC donde almacenamos recursos de AWS como instancias EC2 y bases de datos.

* Route table: Las tablas de ruteo contienen conjuntos de reglas, denominadas rutas, que se usanpara determinar
adónde se dirige el tráfico de red. Cada subred de la VPC debe estar asociada a una tabla de ruteo. La tabla controla el direccionamiento de la subred. La subred solo puede asociarse a una tabla de ruteo a la vez; sin embargo, puede asociar varias subredes a la misma tabla de ruteo.

* Internet Gateway: Un gateway de Internet es un componente de la VPC de escalado horizontal, redundante y de alta disponibilidad que permite la comunicación entre las instancias de su VPC e Internet.

* NAT Gateway: Puede utilizar una gateway de conversión de las direcciones de red (NAT) para permitir
a las instancias de la subred privada conectarse a Internet o a otros servicios de AWS a la vez que se impide a Internet iniciar una conexión a esas mismas instancias.

* NACL: Una lista de control de acceso (ACL) de red es una capa de seguridad opcional para su VPC que actúa
como firewall para controlar el tráfico entrante y saliente de una o varias subredes.


## Videos de la clase

* [Explicación del Proyecto de servicios en la nube de AWS](https://youtu.be/cTpAPXJHLgg) 
* [1a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/nGXdcVtG_LU)
* [2a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/zlMxB-icY7I)
* [3a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/xfdieI_E8zw)
* [4a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/mRs0Ftb_sqI)
* [5a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/iXNh7_x1BnM)
* [6a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/w6WIBN8m2F0)
* [7a sesión de dudas sobre el Proyecto de Cloud](https://youtu.be/jLWDXAxQv-U)


## Rúbrica de calificación 
Elementos a evaluar.


